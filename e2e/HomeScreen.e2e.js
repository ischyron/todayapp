describe('WHEN HomeScreen is loaded', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have input github username to be empty', async () => {
    await expect(element(by.id('gitHubUserName'))).toBe('');
  });

  describe('WHEN GitHandle is typed in', () => {
    xit('should displau github user details', async () => {
      await expect(element(by.id('gitHubUserName'))).toBe('');
    });
  });
});
