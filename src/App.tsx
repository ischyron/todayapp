import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import configureStore from './redux/store';
// import {Text} from 'react-native';
import HomeScreen from './screens/home/HomeScreen';

const Stack = createNativeStackNavigator();
const store = configureStore();

export default function App() {
  return (
    <Provider store={store}>
      {/* <Text>Debug</Text> */}
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{title: 'Today'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

// if (__DEV__) {
//   import('../ReactronConfig').then(() => console.log('Reactotron Configured'));
// }
