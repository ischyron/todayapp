import {combineEpics} from 'redux-observable';
import {combineReducers} from 'redux';
import {catchError} from 'rxjs';

import {pingReducer as ping, pingEpic} from './ping';
import {userReducer as user, fetchUserEpic} from './user';

export const rootEpic = (action$, store$, dependencies) =>
  combineEpics(pingEpic, fetchUserEpic)(action$, store$, dependencies).pipe(
    catchError((error, source) => {
      console.error(error);
      return source;
    }),
  );

export const rootReducer = combineReducers({
  ping,
  user,
});
