import {ajax} from 'rxjs/ajax';
import {of, map, mergeMap, catchError} from 'rxjs';
import {ofType} from 'redux-observable';

type UserDetails = {
  login: String;
  avatar_url: String;
  name: String;
  location: string;
};

export enum UserActionTypes {
  FETCH_USER = 'FETCH_USER',
  FETCH_USER_FULFILLED = 'FETCH_USER_FULFILLED',
}

export interface IUserFetchAction {
  type: UserActionTypes.FETCH_USER;
  payload: String;
}
export interface IUserFetchFullfilledAction {
  type: UserActionTypes.FETCH_USER_FULFILLED;
  payload: UserDetails;
}

// action creators
export const fetchUser = (loginUserName: string): IUserFetchAction => ({
  type: UserActionTypes.FETCH_USER,
  payload: loginUserName,
});

export const fetchUserFulfilled = (
  payload: UserDetails,
): IUserFetchFullfilledAction => ({
  type: UserActionTypes.FETCH_USER_FULFILLED,
  payload,
});

// epic
export const fetchUserEpic = action$ =>
  action$.pipe(
    ofType(UserActionTypes.FETCH_USER),
    mergeMap(action =>
      ajax
        .getJSON(`https://api.github.com/users/${action.payload}`)
        .pipe(catchError(() => of({}))) //@todo dont swallow errors
        .pipe(
          map(
            (response: UserDetails) =>
              response !== {} && fetchUserFulfilled(response),
          ),
        ),
    ),
  );

export const userReducer = (state = {}, action) => {
  switch (action.type) {
    case UserActionTypes.FETCH_USER:
      return {
        ...state,
      };
    case UserActionTypes.FETCH_USER_FULFILLED:
      const {name, avatar_url} = action.payload;
      return {
        ...state,
        userName: name || '',
        avatarUrl: avatar_url,
      };

    default:
      return state;
  }
};
