import React from 'react';
import {connect} from 'react-redux';
import {Text, TextInput, View, Image} from 'react-native';
// import {fetchUser} from '@redux-modules/users';
import {fetchUser} from '../../redux/modules/user';
import styles from './styles';

function HomeScreen(props) {
  const {userName, avatarUrl, getUser} = props;
  const Image_Http_URL = {
    uri: avatarUrl,
  };
  const [gitHubHandle, onChangeText] = React.useState('');

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text testID="gitHubHandleInputLabel">GitHub handle</Text>
      <TextInput
        testID="gitHubHandleInputLabel"
        style={styles.input}
        onChange={() => getUser(gitHubHandle)}
        onChangeText={onChangeText}
        value={gitHubHandle}
      />
      <Text testID="gitHubUserName">{userName}</Text>
      {avatarUrl && (
        <Image
          source={Image_Http_URL}
          style={{height: 160, width: 160, resizeMode: 'stretch', margin: 5}}
        />
      )}
    </View>
  );
}

function mapStateToProps(state) {
  return {
    userName: state.user.userName,
    avatarUrl: state.user.avatarUrl,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getUser: (gitHubHandle: string) => dispatch(fetchUser(gitHubHandle)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
